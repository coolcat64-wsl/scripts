#!/bin/bash

#--SYNOPSIS---------------------------------------------------------------------
   
# WSL 2 script to mount an external hard drive available via Windows and run
# rsync to backup WSL 2 files. This script requires sudo privileges.

#--GLOBAL VARIABLES-------------------------------------------------------------
# Mount Nargothrond, expected on Windows drive F:\
# sudo mount -t drvfs e: /mnt/e
mount_options="-t drvfs -o metadata"
mount_drive_name="Nargothrond"
mount_drive="F:"
mount_point="/mnt/f/"
mnt_cmd="sudo mount $mount_options $mount_drive $mount_point"

# Unmount command
# sudo umount /mnt/f/
unmnt_cmd="sudo umount $mount_point"

# Run rsync on the home directory for backup
# sudo rsync -av /home /mnt/f/kali-linux/
distro_filename="distro-name.txt"
# Set the default distribution name
distro_name="kali-linux"
rsync_options="-av --no-specials --no-devices"
rsync_src="/home"
rsync_dest="${mount_point}${distro_name}/"
rsync_cmd="sudo rsync $rsync_options $rsync_src $rsync_dest"

#--FUNCTION DEFINITIONS---------------------------------------------------------

function queryYesOrQuit()
{
   while true; do
      read -p "is this correct? (y/n): " yn
      case $yn in
         [Yy]* ) break;;
         [Nn]* ) printf "Exiting\n"; exit;;
         * ) printf "Exiting\n"; exit;;
      esac
   done
}

function trim()
{
   local __resultvar=$1
   local var=$2

   # This isn't needed if using 'read -r', but it is here for robustness
   # https://stackoverflow.com/questions/369758/how-to-trim-whitespace-from-a-bash-variable
   # remove leading whitespace characters
   var="${var#"${var%%[![:space:]]*}"}"
   # remove trailing whitespace characters
   var="${var%"${var##*[![:space:]]}"}"   

   # https://stackoverflow.com/questions/15244801/how-to-remove-escape-char-from-string-in-bash-using-sed 
   # remove escape characters
   var="${var//\\}"

   # https://www.linuxjournal.com/content/return-values-bash-functions
   if [[ "$__resultvar" ]]; then
      eval $__resultvar="'$var'"
   else
      echo "$var"
      #printf '%s' "$var"
   fi
}

#--EXECUTION--------------------------------------------------------------------

# Check if the drive is mounted before trying to mount
# https://stackoverflow.com/questions/9422461/check-if-directory-mounted-with-bash
if [[ ! $(findmnt -M "$mount_point") ]]; then
   printf "Drive $mount_drive_name on $mount_drive will be mounted to $mount_point..."
   queryYesOrQuit

   printf "Mounting $mount_drive at $mount_point...\n"

   # Attempt to mount
   echo $mnt_cmd
   if ! ($mnt_cmd); then
      # The mount fails
      printf "\nFailed to mount $mount_drive at $mount_point...exiting\n"
      exit 1
   else
      printf "Waiting for drive to spin up..."
      sleep 7
      printf "done\n\n"
   fi
fi

# Check if there are files at the mount point. 
# https://stackoverflow.com/questions/91368/checking-from-shell-script-if-a-directory-contains-files
printf "Checking for files in $mount_point..."
files=$(shopt -s nullglob dotglob; echo $mount_point*)
if (( ${#files} )); then
   # If there are files, the mount succeeded
   printf "found these files: \n"
   printf "$files\n\n"
else 
   # There are no files at the mount point, error out
   printf "no files found.\n\n"
   printf "The directory $mount_point is empty, does not exist, or is a file! Exiting\n"
   exit 1
fi

# Get the distribution name for rsync destination
if test -f "$distro_filename"; then
   # Read the first line of the distribution file
   # https://stackoverflow.com/questions/2439579/how-to-get-the-first-line-of-a-file-in-a-bash-script
   # Read the first word. Read splits on the IFS (Input Field Separator); default is space.
   read -r word < $distro_filename

   # Trim whitespace and escape characters, set $distro_name
   trim distro_name ${word}
   printf "File $distro_filename indicates target '$distro_name'..."
   # Set the new rsync destination directory
   rsync_dest="${mount_point}${distro_name}/"
   # Set the new rsync command
   rsync_cmd="sudo rsync $rsync_options $rsync_src $rsync_dest"
else
   # Use the default distribution name and rsync target
   printf "File $distro_filename does not exist..."
fi

printf "using '$distro_name'\n\n"

## Check if rsync destination exists
#if [ ! -d "$rsync_dest" ]; then
#   printf "Destination directory $rsync_dest does not exist. Creating..."
#   mkdir -p "$rsync_dest"
#fi

# Create the destination directory if it does not exist
mkdir_cmd="mkdir -p $rsync_dest"
echo $mkdir_cmd
if ! ($mkdir_cmd); then
   printf "Failed to make directory $rsync_dest...exiting\n"
   exit 1
fi

printf "Run rsync from $rsync_src to $rsync_dest..."
queryYesOrQuit
# Kick off rsync
printf "\nExecuting rsync...\n\n"
echo $rsync_cmd
$rsync_cmd

