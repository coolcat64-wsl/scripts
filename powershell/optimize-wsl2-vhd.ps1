<#
.SYNOPSIS
    Optimize (shrink) WSL 2 .vhdx filesystem in Windows 10 Professional.
    Inspired by this open issue: https://github.com/microsoft/WSL/issues/4699

.DESCRIPTION
    Shuts down all WSL instances and runs optimization on the VHD file system
    for a specified WSL 2 distribution. The Optimize-VHD command is only
    supported by Windows 10 Professional. This script must be run in PowerShell
    with Administrator privileges.  Run `wsl` or your favorite terminal to
    resume use.

.PARAMETER -Target <System.String[]>
    Specifies the WSL 2 distribution target.  The script currently supports VHD
    optimization for the Ubuntu-20.04 and kali-linux distribution targets
    installed on Windows 10 Professional.

.PARAMETER -WhatIf <Switch>
    Specifies whether to run the optimization cmdlet with the -WhatIf flag.
    This is used for testing.

.INPUTS
    The following .vhdx file must exist:
        $env:LOCALAPPDATA\Packages\TARGET_DISTRO_FOLDERNAME\LocalState\ext4.vhdx

    Let $env:LOCALAPPDATA be the %LOCALAPPDATA% environment vairable.

    Let TARGET_DISTRO_FOLDERNAME be the folder where the WSL 2 distribution VHD
    is stored on disk. The folder names are specific to the local installation,
    and should be set correctly in the $sTargetFolderHash lookup table.

    Examples of TARGET_DISTRO_FOLDERNAME folders:
        CanonicalGroupLimited.UbuntuonWindows_79rhkp1fndgsc
        CanonicalGroupLimited.Ubuntu20.04onWindows_79rhkp1fndgsc

.OUTPUTS
    None.

.NOTES
    Version:        1.0
    Author:         Michael DeClerck
    Creation Date:  11/2/2020
    Purpose/Change: Initial script development

.REMARKS
    Don't run this too often because it will wear out the disk.

    The following dependencies exist:
        1.) Windows 10 Professional with all Hyper-V features enabled.
        2.) Installed WSL 2 distribution targets specified in the $sTargetFolderHash. 
        3.) PowerShell script running policy enabled. See below.

    To set a safe PowerShell script running policy, open a PowerShell terminal
    with Administrator privileges and run the following command:
      Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser
    You will have to select 'Y' in order to confirm the policy change.

    Future improvements:
        1.) Support Windows 10 Home: 
            https://github.com/microsoft/WSL/issues/4699#issuecomment-627133168
        2.) Use Start-Process to elevate to Administrator (via -Verb RunAs)
        3.) Dynamically search for TARGET_DISTRO_FOLDERNAME folders

.EXAMPLE
    powershell.exe .\optimize-wsl2-vhd.ps1 -Target Ubuntu-20.04

.EXAMPLE
    powershell.exe .\optimize-wsl2-vhd.ps1 -Target kali-linux -WhatIf
#>
[CmdletBinding()] # Operates like a compiled C# cmdlet
Param(  [Parameter(Mandatory=$True)]    [String]$Target,
        [Parameter(Mandatory=$False)]   [Switch]$WhatIf)

#---------------------------------------------------------[Initialisations]--------------------------------------------------------

#----------------------------------------------------------[Declarations]----------------------------------------------------------

# Script version
$sScriptVersion = "1.0"
$sScriptName = $MyInvocation.MyCommand.Name
$sRunDate = Get-Date

# The distribution root folder
$sDistroRoot = "$env:LOCALAPPDATA\Packages\"
# The .vhdx filename
$sVhdFilename = "ext4.vhdx"
# The VHD optimization cmdlet supported by Windows 10 Professional
$sOptimizeCmd = 'Optimize-VHD'
# The supported target distribution to folder hash
$sTargetFolderHash = @{
    "Ubuntu-18.04" = "CanonicalGroupLimited.UbuntuonWindows_DUMMYPATH"; # Non-existing folder for testing
    "Ubuntu-20.04" = "CanonicalGroupLimited.Ubuntu20.04onWindows_79rhkp1fndgsc";
    "kali-linux" = "KaliLinux.54290C8133FEE_ey8k8hqnwqnmg"
}

# Log file info
#$sLogPath = ".\logs"
##$sLogPath = (Test-Path -Path ".\logs" -PathType Container) ? ".\logs" : ".\"
#$sLogName = "program.log"
#$sLogFile = Join-Path -Path $sLogPath -ChildPath $sLogName

#-----------------------------------------------------------[Functions]------------------------------------------------------------

function isCommand($cmdname)
{
    return [bool](Get-Command -Name $cmdname -ErrorAction SilentlyContinue)
}

function isAdmin
{
    return [bool](([Security.Principal.WindowsPrincipal] `
            [Security.Principal.WindowsIdentity]::GetCurrent() `
            ).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator))
}

#-----------------------------------------------------------[Execution]------------------------------------------------------------

# Print script information
Write-Output "--------------------------------------------------------------------------------"
Write-Output "$($sScriptName) v$($sScriptVersion), $sRunDate"
Write-Output "--------------------------------------------------------------------------------"

# Check if the specified $Target parameter is supported
if ($False -eq $sTargetFolderHash.ContainsKey($Target))
{
    # There is no distribution folder for the spiecified target
    $sSupportedTargets = $sTargetFolderHash.keys 
    $sMessage = "Invalid target `"$Target`" specified. `
    Currently supported targets are: $sSupportedTargets" 
    Write-Error -Message $sMessage -Category NotImplemented -TargetObject $Target `
         -Exception([System.Management.Automation.ParameterBindingException]::new()) -ErrorAction Stop
    # Stop the script
    exit
}

# Get the distribution folder for the $Target from the hash
$sDistroFolder = $sTargetFolderHash[$Target]
# Join the distribution folder path to get the VHD file path
$sVhdFile = Join-Path -Path $sDistroRoot -ChildPath "$sDistroFolder\LocalState\$sVhdFilename"

# Check for an existing VHD file to optimize for the specified $Target
if ($False -eq (Test-Path -Path $sVhdFile -PathType leaf))
{
    $sMessage = "Invalid path: $sVhdFile. `
    Is $Target installed for WSL 2 and the correct folder specified in the lookup table?" 
    Write-Error -Message $sMessage -Category ReadError -CategoryTargetName $sVhdFile `
        -Exception([System.IO.FileNotFoundException]::new()) -ErrorAction Stop
    # Stop the script
    exit
}

# Check for Administrator privileges
if ($False -eq (isAdmin))
{
    $sMessage = "Administrator privileges are required to run the $sOptimizeCmd command." 
    Write-Error -Message $sMessage -Category PermissionDenied -TargetObject $sOptimizeCmd `
        -Exception([System.Security.Authentication.AuthenticationException]::new()) -ErrorAction Stop
    # Stop the script
    exit
}

# Check that the $sOptimizeCmd is supported
if ($False -eq (isCommand($sOptimizeCmd)))
{
    $sMessage = "The command $sOptimizeCmd is not supported! `
    Do you have Windows 10 Professional installed with all Hyper-V features enabled?" 
    Write-Error -Message $sMessage -Category ObjectNotFound -TargetObject $sOptimizeCmd `
        -Exception([System.Management.Automation.CommandNotFoundException]::new()) -ErrorAction Stop
    # Stop the script
    exit   
}

# Used for testing purposes. Avoid running repeated optimizations commands
# during test to save the local disk!
if ($WhatIf) {$WhatIfString = "-WhatIf"}

# Build the full command expression string
$sFullCommand = $("$sOptimizeCmd -Path $sVhdFile -Mode full $WhatIfString")
#$sStartProcess = "Start-Process -FilePath 'PowerShell' -NoNewWindow -Wait -Verb RunAs -ArgumentList $sFullCommand"

Write-Output "Shutting down WSL..."
# Shutdown all WSL distributions and instances
wsl --shutdown
wsl --list --verbose
Write-Output "Optimizing $sVhdFile..."  
# Evaluate the command in the string
#Write-Output $sFullCommand
Invoke-Expression $sFullCommand
#Start-Process -FilePath 'PowerShell' -NoNewWindow -Wait -Verb RunAs -ArgumentList "Invoke-Expression $sFullCommand"
