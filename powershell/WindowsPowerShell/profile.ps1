# This profile was created from scratch using the following commands:
# > if (!(Test-Path -Path $PROFILE.CurrentUserAllHosts)) {
# >   New-Item -ItemType File -Path $PROFILE.CurrentUserAllHosts -Force
# > }
# Reference: https://dev.to/ofhouse/add-a-bash-like-autocomplete-to-your-powershell-4257

# To get this profile script to execute on startup, type the following in an
# admin terminal:
# > Set-ExecutionPolicy RemoteSigned

# NOTE: MenuComplete is invoked with Ctrl+Space by default. MenuComplete shows
# naivgable menu, but Complete is a lot more like traditional Bash behavior
Set-PSReadlineKeyHandler -Key Tab -Function Complete

# Set the default value of -Confirm parameter to $True for Remove-Item
$PSDefaultParameterValues["Remove-Item:Confirm"]=$true

# Install PSColor module with command "Install-Module PSColor" for fancy colors
Import-Module PSColor

# New-Alias -Name <alias> -Value <aliased-command>
Set-Alias -Name touch -Value New-Item   # Make a new file

#Function Remove-ItemConfirm {Remove-Item -Confirm$true -Path}
#Set-Alias -Name rm -Value Remove-ItemConfirm -Option AllScope
